# Docker Template

This is a simple template project with a working generic pipeline to push docker built
images to both [DockerHub](https://hub.docker.com) and the local gitlab.com registry
of the project itself.

## Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Docker](https://www.docker.com)
- [Docker Documentation](https://docs.docker.com)

If you are new to docker, please use [stack_overflow](https://stackoverflow.com) and
Docker's documentation.

## What's contained in this project

We provide a simple Hello World docker image running on several architectures and
severals linux distribution.

## Usage

To use this, update the Dockerfile and add the tags/architectures/base-images you want
in the file ./images.yml.

Make a really simple change first to commit only what you want.

Then update `.gitlab-ci.yml` and read the comments inside it to improve the
efficiency of your image generation.

### How images are pushed and when?

On main (DEFAULT_BRANCH), images are first built and pushed with the `docker-build` job
(see `.gitlab-ci.yml` file) to the registry's project with the tag built like this:
  - <project_slug>:<base_image_name>-<base_image_tag>-latest
  - <project_slug>:<base_image_name>-<base_image_tag>-<git log --format='%h'>

If you make a tag, then it will be pushed as:
  - <project_slug>:<base_image_name>-<base_image_tag>-<tag>

Then, it will be pushed to all the defined `REGISTRIES`. By default, we put `docker.io`
to push it to DockerHub, but you must define correctly 2 ENV Variables in the Gitlab's
group containing your project or in the project itself for this to work:
  - `DOCKERHUB_USER`
  - `DOCKERHUB_PASSWORD`

By default, we use `DOCKERHUB_USER` to set `DOCKERHUB_PREFIX` but you can put here your
company or something like that.

You can also add some other registries by separating it with space in the `REGISTRIES`
variable of the docker-push job. You will hage to then define the following variables
for a registry you call `MY_REGISTRY`:
  - MY_REGISTRY: the base URL of the registry (https://index.docker.io/v1/ for dockerhub for example)
  - MY_REGISTRY_USER: a valid username
  - MY_REGISTRY_PASSWORD: a valid password/token to authenticate

    -> prefere access token: https://docs.docker.com/security/for-developers/access-tokens/

    -> Since we run this job on protected branch/tag only, you can check the box to protect the variable
  - MY_REGISTRY_PREFIX: the prefix for the image to be pushed to (in docker it can be the same as the username)

    -> Image will be pushed like this `docker push <registry> $*_PREFIX/<image>:<tag>`

Example:
```yaml
docker-push:
  image: docker:cli
  stage: deploy
  variables:
  REGISTRIES: DOCKERHUB MY_REGISTRY
    DOCKERHUB: docker.io
    DOCKERHUB_PREFIX: $DOCKERHUB_USER
    MY_REGISTRY: myregistry.io
    MY_REGISTRY_PREFIX: ''
    MY_REGISTRY_USER: my_username
```

Then define MY_REGISTRY_PASSWORD as a protected variable. Don't forget to define protected tags.

### Docker HUB

To deploy on Docker Hub, this will be done only on `deploy` job and if DOCKER_TOKEN,
DOCKER_BASE_NAME (basically where your token has the right to deploy) environment
variables are set.

## Support

First get support on [stack_overflow](https://stackoverflow.com), you can tag
me [@rlaures](https://stackoverflow.com/users/3129859/rlaures) or recruit me or
my team.

## Need more images or signal a bug/flaw

Use the [issue tracking system of Gitlab](https://gitlab.com/liant-sasu/templates/docker-template/-/issues) please.
Follow the contribution code and please be cordial.

## Contributing

See [Contribution file](./CONTRIBUTING.md) to contribute (basically, create an issue
[here](https://gitlab.com/liant-sasu/docker-template/-/issues/new) and be clear).

## Authors and acknowledgment

Contributors are from [Liant](https://gitlab.com/liant-sasu) (at this moment).

## License

[MIT](./LICENSE)
