#! /usr/bin/env ruby
# frozen_string_literal: true

require 'English'

require_relative 'tools'
require_relative 'images'
require_relative 'registries'

## This script reads the images.yml file and build the images you ask

# It will basically use the Dockerfile, but for specific distributions or image relative
# stuffs, it will first look for a Dockerfile.<image_name> file.

## First ensure the usability of the multi-arch builder
system('docker run --rm --privileged tonistiigi/binfmt --install all')
unless $CHILD_STATUS.success?
  warn 'Unable to set Emulator'
  exit 1
end
system('docker context inspect multi-plat')
unless $CHILD_STATUS.success?
  warn '=> Unable to find the builder\'s context, building it'
  system('docker context create multi-plat')
  unless $CHILD_STATUS.success?
    warn '|-> Unable to create the multi-plat\'s context'
    exit 1
  end
end
ENV.delete 'DOCKER_HOST' if ENV.include? 'DOCKER_HOST'
system('docker context use multi-plat')
unless $CHILD_STATUS.success?
  warn 'Unable to select the multi-plat'
  exit 1
end
output = `docker buildx inspect multi-plat-builder`
if !$CHILD_STATUS.success? || %r{Driver:\s*docker-container}.match(output).nil?
  if $CHILD_STATUS.success?
    puts '=> Driver seams to be wrong, re-creating it'
    system('docker buildx rm --force multi-plat-builder')
  else
    puts '=> creating it...'
  end
  system('docker buildx create --driver=docker-container --name multi-plat-builder')
  unless $CHILD_STATUS.success?
    warn '|-> Unable to create the builder'
    exit 1
  end
end
system('docker buildx use multi-plat-builder')
unless $CHILD_STATUS.success?
  warn 'Unable to use the builder'
  exit 1
end
system('docker buildx inspect --bootstrap')
unless $CHILD_STATUS.success?
  warn 'Unable to activate the builder'
  exit 1
end

run_system('Login to Gitlab CI registry') do
  docker_login_on('CI_REGISTRY')
end

# Get Platforms we have to build on that job
BUILD_PLATFORMS = ENV.fetch('BUILD_PLATFORMS', '').split(' ')

# Main loop work.
IMAGE_DESCRIPTIONS.each do |image, tags|
  puts '---------------'
  puts "#{image}:"

  tags.each do |tag, platforms|
    puts '---------------'
    puts "  #{tag}:"
    puts '---------------'

    # Select the platform(s) we will build in this job:
    platforms.select! { |platform| BUILD_PLATFORMS.include?(platform) } unless BUILD_PLATFORMS.empty?
    platforms.sort!
    puts " -> Building for platforms: #{platforms.join(', ')}"

    # Build the images and push to CI_REGISTRY
    run_system(
      'Build',
      [
        build_cmd(
          image, tag, platforms,
          path_to_image_for('CI_REGISTRY'),
          build_by_platforms_suffix(platforms)
        )
      ]
    )

    # # retag to CI_REGISTRY
    # local_images = tag_params(image, tag)
    # cmds = tag_params(image, tag, path_to_image_for('CI_REGISTRY')).map.each_with_index do |ci_image, i|
    #   "docker tag #{local_images[i]} #{ci_image}"
    # end
    # run_system('Rename tags', cmds)

    # run_system('Gitlab CI Push') do
    #   docker_push_on('CI_REGISTRY')
    # end
  end
end
